"use strict";



define('week-04/adapters/application', ['exports', 'ember-data'], function (exports, _emberData) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _emberData.default.JSONAPIAdapter.extend({
        host: 'http://localhost:4201'
    });
});
define('week-04/app', ['exports', 'week-04/resolver', 'ember-load-initializers', 'week-04/config/environment'], function (exports, _resolver, _emberLoadInitializers, _environment) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });


  var App = Ember.Application.extend({
    modulePrefix: _environment.default.modulePrefix,
    podModulePrefix: _environment.default.podModulePrefix,
    Resolver: _resolver.default
  });

  (0, _emberLoadInitializers.default)(App, _environment.default.modulePrefix);

  exports.default = App;
});
define('week-04/components/number-game', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Component.extend({
        playing: false,
        correct: false,
        guesses: 0,
        guessValue: 0,
        limits: null,

        actions: {
            start: function start() {
                this.set('playing', true);
                this.set('correct', false);
                this.set('guessValue', Math.floor(Math.random() * 100) + 1);
                this.set('guesses', 1);
                this.set('limits', { 'min': 1, 'max': +1 });
            },

            lower: function lower() {
                var limit = this.get('limits');
                limit.max = this.get('guessValue');
                this.set('guessValue', limit.min + Math.floor((limit.max - limit.min) / 2));
                this.set('guesses', this.get('guesses') + 1);
            },

            higher: function higher() {
                var limit = this.get('limits');
                limit.min = this.get('guessValue');
                this.set('guessValue', limit.min + Math.floor((limit.max - limit.min + 1) / 2));
                this.set('guesses', this.get('guesses') + 1);
            },
            correct: function correct() {
                this.set('correct', true);
            },
            'save-highscore': function saveHighscore() {
                var action = this.get('on-save-highscore');
                if (action !== undefined) {
                    action(this.get('player_name'), this.get('guesses'));
                }
            }

        }
    });
});
define("week-04/components/tic-tac-toe", ["exports"], function (exports) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Component.extend({
        playing: false,
        winner: undefined,
        draw: false,

        //Add sound function
        desktop: true,
        init: function init() {
            this._super.apply(this, arguments);
            createjs.Sound.registerSound("assets/sounds/click.wav", "place-marker");
            createjs.Sound.registerSound("assets/sounds/falling.mp3", "falling");
            createjs.Sound.registerSound("assets/sounds/cheer.wav", "cheer");
            var component = this;
            document.addEventListener("deviceready", function () {
                component.set('desktop', false);
                if (shake) {
                    shake.startWatch(function () {
                        component.send('start');
                    });
                }
            }, false);
        },

        didInsertElement: function didInsertElement() {
            var stage = new createjs.Stage(this.$('#stage')[0]);
            //Draw the game board
            var board = new createjs.Shape();
            var graphics = board.graphics;
            graphics.beginFill('#ffffff');
            graphics.drawRect(0, 99, 300, 2);
            graphics.drawRect(0, 199, 300, 2);
            graphics.drawRect(99, 0, 2, 300);
            graphics.drawRect(199, 0, 2, 300);
            board.x = 40;
            board.y = 40;
            board.alpha = 0;
            this.set('board', board);
            stage.addChild(board);

            //Create Markers
            var markers = {
                'x': [],
                'o': []
            };
            for (var x = 0; x < 5; x++) {
                var circleMarker = new createjs.Shape();
                graphics = circleMarker.graphics;
                graphics.beginStroke('#66ff66');
                graphics.setStrokeStyle(10);
                graphics.drawCircle(0, 0, 30);
                circleMarker.visible = false;
                stage.addChild(circleMarker);
                markers.o.push(circleMarker);

                var crossMarker = new createjs.Shape();
                graphics = crossMarker.graphics;
                graphics.beginStroke('#6666ff');
                graphics.setStrokeStyle(10);
                graphics.moveTo(0, 0);
                graphics.lineTo(40, 40);
                graphics.moveTo(0, 40);
                graphics.lineTo(40, 0);
                crossMarker.visible = false;
                stage.addChild(crossMarker);
                markers.x.push(crossMarker);
            }

            this.set('markers', markers);
            this.set('stage', stage);

            //Update the drawing
            createjs.Ticker.addEventListener("tick", stage);
        },

        willDestroyElement: function willDestroyElement() {
            this._super.apply(this, arguments);
            if (shake) {
                shake.stopWatch();
            }
        },

        click: function click(ev) {

            if (this.get('playing') && !this.get('winner')) {
                if (ev.target.tagName.toLowerCase() === "canvas" && ev.originalEvent.offsetX >= 40 && ev.originalEvent.offsetY >= 40 && ev.originalEvent.offsetX < 340 && ev.originalEvent.offsetY < 340) {

                    var x = Math.floor((ev.originalEvent.offsetX - 40) / 100);
                    var y = Math.floor((ev.originalEvent.offsetY - 40) / 100);
                    var state = this.get('state');

                    if (!state[x][y]) {
                        createjs.Sound.play("place-marker");
                        var player = this.get("player");
                        state[x][y] = player;

                        var move_count = this.get('moves')[player];
                        var marker = this.get('markers')[player][move_count];
                        marker.visible = true;
                        if (player == 'x') {
                            marker.x = 70 + x * 100;
                            marker.y = 70 + y * 100;
                        } else {
                            marker.x = 90 + x * 100;
                            marker.y = 90 + y * 100;
                        }

                        this.check_winner();
                        this.get('moves')[player] = move_count + 1;

                        if (player == 'x') {
                            this.set('player', 'o');
                        } else {
                            this.set('player', 'x');
                        }
                        if (!this.get('winner') && window.plugins && window.plugins.toast) {
                            window.plugins.toast.showShortBottom(this.get('player').toUpperCase() + 'to play next');
                        }
                        //this.get('stage').update();
                    }
                }
            }
        },

        check_winner: function check_winner() {
            var patterns = [[[0, 0], [1, 1], [2, 2]], [[0, 2], [1, 1], [2, 0]], [[0, 0], [0, 1], [0, 2]], [[1, 0], [1, 1], [1, 2]], [[2, 0], [2, 1], [2, 2]], [[0, 0], [1, 0], [2, 0]], [[0, 1], [1, 1], [2, 1]], [[0, 2], [1, 2], [2, 2]]];
            var state = this.get('state');
            for (var pidx = 0; pidx < patterns.length; pidx++) {
                var pattern = patterns[pidx];
                var winner = state[pattern[0][0]][pattern[0][1]];
                if (winner) {
                    for (var idx = 1; idx < pattern.length; idx++) {
                        if (winner != state[pattern[idx][0]][pattern[idx][1]]) {
                            winner = undefined;
                            break;
                        }
                    }
                    if (winner) {
                        this.set('winner', winner);
                        createjs.Sound.play("cheer");
                        break;
                    }
                }
            }
            if (!this.get('winner')) {
                var draw = true;
                for (var x = 0; x <= 2; x++) {
                    for (var y = 0; y <= 2; y++) {
                        if (!state[x][y]) {
                            draw = false;
                            break;
                        }
                    }
                }
                this.set('draw', draw);
            }
        },

        actions: {
            start: function start() {
                if (window.plugins && window.plugins.toast) {
                    window.plugins.toast.showShortBottom('X to play next');
                }
                var board = this.get('board');
                board.alpha = 0;

                if (this.get('playing')) {
                    var markers = this.get('markers');
                    for (var idx = 0; idx < 5; idx++) {
                        createjs.Tween.get(markers.x[idx]).to({ y: 600 }, 500);
                        createjs.Tween.get(markers.o[idx]).to({ y: 600 }, 500);
                    }
                    createjs.Sound.play("falling");
                    createjs.Tween.get(board).wait(500).to({ alpha: 1 }, 1000);
                } else {
                    createjs.Tween.get(board).to({ alpha: 1 }, 1000);
                }

                this.set('playing', true);
                this.set('winner', undefined);
                this.set('draw', false);
                this.set('state', [[undefined, undefined, undefined], [undefined, undefined, undefined], [undefined, undefined, undefined]]);

                this.set('moves', { 'x': 0, 'o': 0 });
                this.set('player', 'x');
                var markers = this.get('markers');
                for (var idx = 0; idx < 5; idx++) {
                    markers.x[idx].visible = false;
                    markers.o[idx].visible = false;
                }
                //this.get('stage').update();
            }
        }

    });
});
define('week-04/components/welcome-page', ['exports', 'ember-welcome-page/components/welcome-page'], function (exports, _welcomePage) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _welcomePage.default;
    }
  });
});
define('week-04/controllers/game', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Controller.extend({
        actions: {
            'save-highscore': function saveHighscore(name, score) {
                var controller = this;
                var highscore = controller.store.createRecord('highscore', {
                    name: name,
                    score: score
                });
                highscore.save().then(function () {
                    controller.store.unloadAll();
                    controller.transittionToRoute('highscores');
                });
            }
        }
    });
});
define('week-04/helpers/app-version', ['exports', 'week-04/config/environment', 'ember-cli-app-version/utils/regexp'], function (exports, _environment, _regexp) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.appVersion = appVersion;
  function appVersion(_) {
    var hash = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    var version = _environment.default.APP.version;
    // e.g. 1.0.0-alpha.1+4jds75hf

    // Allow use of 'hideSha' and 'hideVersion' For backwards compatibility
    var versionOnly = hash.versionOnly || hash.hideSha;
    var shaOnly = hash.shaOnly || hash.hideVersion;

    var match = null;

    if (versionOnly) {
      if (hash.showExtended) {
        match = version.match(_regexp.versionExtendedRegExp); // 1.0.0-alpha.1
      }
      // Fallback to just version
      if (!match) {
        match = version.match(_regexp.versionRegExp); // 1.0.0
      }
    }

    if (shaOnly) {
      match = version.match(_regexp.shaRegExp); // 4jds75hf
    }

    return match ? match[0] : version;
  }

  exports.default = Ember.Helper.helper(appVersion);
});
define('week-04/helpers/pluralize', ['exports', 'ember-inflector/lib/helpers/pluralize'], function (exports, _pluralize) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = _pluralize.default;
});
define('week-04/helpers/singularize', ['exports', 'ember-inflector/lib/helpers/singularize'], function (exports, _singularize) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = _singularize.default;
});
define('week-04/initializers/app-version', ['exports', 'ember-cli-app-version/initializer-factory', 'week-04/config/environment'], function (exports, _initializerFactory, _environment) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });


  var name = void 0,
      version = void 0;
  if (_environment.default.APP) {
    name = _environment.default.APP.name;
    version = _environment.default.APP.version;
  }

  exports.default = {
    name: 'App Version',
    initialize: (0, _initializerFactory.default)(name, version)
  };
});
define('week-04/initializers/container-debug-adapter', ['exports', 'ember-resolver/resolvers/classic/container-debug-adapter'], function (exports, _containerDebugAdapter) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = {
    name: 'container-debug-adapter',

    initialize: function initialize() {
      var app = arguments[1] || arguments[0];

      app.register('container-debug-adapter:main', _containerDebugAdapter.default);
      app.inject('container-debug-adapter:main', 'namespace', 'application:main');
    }
  };
});
define('week-04/initializers/data-adapter', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = {
    name: 'data-adapter',
    before: 'store',
    initialize: function initialize() {}
  };
});
define('week-04/initializers/ember-data', ['exports', 'ember-data/setup-container', 'ember-data'], function (exports, _setupContainer) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = {
    name: 'ember-data',
    initialize: _setupContainer.default
  };
});
define('week-04/initializers/export-application-global', ['exports', 'week-04/config/environment'], function (exports, _environment) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.initialize = initialize;
  function initialize() {
    var application = arguments[1] || arguments[0];
    if (_environment.default.exportApplicationGlobal !== false) {
      var theGlobal;
      if (typeof window !== 'undefined') {
        theGlobal = window;
      } else if (typeof global !== 'undefined') {
        theGlobal = global;
      } else if (typeof self !== 'undefined') {
        theGlobal = self;
      } else {
        // no reasonable global, just bail
        return;
      }

      var value = _environment.default.exportApplicationGlobal;
      var globalName;

      if (typeof value === 'string') {
        globalName = value;
      } else {
        globalName = Ember.String.classify(_environment.default.modulePrefix);
      }

      if (!theGlobal[globalName]) {
        theGlobal[globalName] = application;

        application.reopen({
          willDestroy: function willDestroy() {
            this._super.apply(this, arguments);
            delete theGlobal[globalName];
          }
        });
      }
    }
  }

  exports.default = {
    name: 'export-application-global',

    initialize: initialize
  };
});
define('week-04/initializers/injectStore', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = {
    name: 'injectStore',
    before: 'store',
    initialize: function initialize() {}
  };
});
define('week-04/initializers/store', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = {
    name: 'store',
    after: 'ember-data',
    initialize: function initialize() {}
  };
});
define('week-04/initializers/transforms', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = {
    name: 'transforms',
    before: 'store',
    initialize: function initialize() {}
  };
});
define("week-04/instance-initializers/ember-data", ["exports", "ember-data/initialize-store-service"], function (exports, _initializeStoreService) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = {
    name: "ember-data",
    initialize: _initializeStoreService.default
  };
});
define('week-04/models/highscore', ['exports', 'ember-data'], function (exports, _emberData) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = _emberData.default.Model.extend({
        name: _emberData.default.attr('string'),
        score: _emberData.default.attr('number')
    });
});
define('week-04/resolver', ['exports', 'ember-resolver'], function (exports, _emberResolver) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = _emberResolver.default;
});
define('week-04/router', ['exports', 'week-04/config/environment'], function (exports, _environment) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });


  var Router = Ember.Router.extend({
    location: _environment.default.locationType,
    rootURL: _environment.default.rootURL
  });

  Router.map(function () {
    this.route('game', { path: '/' });
    this.route('highscores');
  });

  exports.default = Router;
});
define('week-04/routes/game', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.Route.extend({});
});
define('week-04/routes/highscores', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Route.extend({
        model: function model() {
            return this.store.findAll('highscore');
        }
    });
});
define('week-04/services/ajax', ['exports', 'ember-ajax/services/ajax'], function (exports, _ajax) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _ajax.default;
    }
  });
});
define("week-04/templates/application", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "fHwL8ecg", "block": "{\"symbols\":[],\"statements\":[[6,\"section\"],[9,\"id\",\"app\"],[7],[0,\"\\n    \"],[6,\"header\"],[7],[0,\"\\n        \"],[6,\"h1\"],[7],[4,\"link-to\",[\"game\"],null,{\"statements\":[[0,\" Tic-Tac-Toe\"]],\"parameters\":[]},null],[8],[0,\"\\n    \"],[8],[0,\"\\n    \"],[6,\"article\"],[7],[0,\"\\n        \"],[1,[18,\"outlet\"],false],[0,\"   \\n    \"],[8],[0,\"\\n    \"],[6,\"footer\"],[7],[0,\"\\n        \"],[6,\"div\"],[9,\"class\",\"float-left\"],[7],[0,\"\\n            Powered by Ember.\\n        \"],[8],[0,\"\\n        \"],[6,\"div\"],[9,\"class\",\"float-right\"],[7],[0,\"\\n           \"],[4,\"link-to\",[\"highscores\"],null,{\"statements\":[[0,\" High-scores \"]],\"parameters\":[]},null],[0,\"\\n        \"],[8],[0,\"\\n    \"],[8],[0,\"\\n\"],[8],[0,\"\\n\"]],\"hasEval\":false}", "meta": { "moduleName": "week-04/templates/application.hbs" } });
});
define("week-04/templates/components/number-game", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "xzrjlX0M", "block": "{\"symbols\":[\"&default\"],\"statements\":[[11,1],[0,\"\\n\\n\"],[4,\"if\",[[20,[\"playing\"]]],null,{\"statements\":[[4,\"if\",[[20,[\"correct\"]]],null,{\"statements\":[[0,\"        \"],[6,\"div\"],[9,\"class\",\"text-center\"],[7],[0,\"Yes! It only took me \"],[1,[18,\"guesses\"],false],[0,\" guesses.\"],[8],[0,\"\\n        \"],[6,\"div\"],[9,\"class\",\"text-center\"],[7],[0,\"Do you want to\"],[6,\"button\"],[3,\"action\",[[19,0,[]],\"start\"]],[7],[0,\"Play again!\"],[8],[8],[0,\"\\n        \"],[6,\"div\"],[9,\"class\",\"text-center\"],[7],[1,[25,\"input\",null,[[\"value\",\"placeholder\"],[[20,[\"player_name\"]],\"Your Name\"]]],false],[6,\"button\"],[3,\"action\",[[19,0,[]],\"save-highscore\"]],[7],[0,\"Save Highscore!\"],[8],[8],[0,\"\\n\"]],\"parameters\":[]},{\"statements\":[[0,\"    \\n    \"],[6,\"div\"],[9,\"class\",\"text-center\"],[7],[0,\"Is it \"],[1,[18,\"guessValue\"],false],[0,\"?\"],[8],[0,\"\\n    \"],[6,\"div\"],[9,\"class\",\"text-center\"],[7],[0,\"\\n        \"],[6,\"button\"],[3,\"action\",[[19,0,[]],\"lower\"]],[7],[0,\"Lower\"],[8],[0,\"\\n        \"],[6,\"button\"],[3,\"action\",[[19,0,[]],\"correct\"]],[7],[0,\"correct\"],[8],[0,\"\\n        \"],[6,\"button\"],[3,\"action\",[[19,0,[]],\"higher\"]],[7],[0,\"Higher\"],[8],[0,\"\\n    \"],[8],[0,\"\\n\"]],\"parameters\":[]}]],\"parameters\":[]},{\"statements\":[[0,\"    \"],[6,\"div\"],[9,\"class\",\"text-center\"],[7],[0,\"think of a number between 1 and 100 and then press \"],[6,\"button\"],[3,\"action\",[[19,0,[]],\"start\"]],[7],[0,\"start !\"],[8],[8],[0,\"\\n\"]],\"parameters\":[]}]],\"hasEval\":false}", "meta": { "moduleName": "week-04/templates/components/number-game.hbs" } });
});
define("week-04/templates/components/tic-tac-toe", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "NHfQzJlu", "block": "{\"symbols\":[\"&default\"],\"statements\":[[11,1],[0,\"\\n\\n\"],[4,\"if\",[[20,[\"playing\"]]],null,{\"statements\":[[4,\"if\",[[20,[\"winner\"]]],null,{\"statements\":[[0,\"        \"],[6,\"div\"],[7],[0,\"\\n            Player \"],[1,[18,\"winner\"],false],[0,\" won!\\n        \"],[8],[0,\"\\n\"]],\"parameters\":[]},null],[4,\"if\",[[20,[\"draw\"]]],null,{\"statements\":[[0,\"        we'll call it a draw.\\n\"]],\"parameters\":[]},null],[4,\"if\",[[20,[\"desktop\"]]],null,{\"statements\":[[0,\"        \"],[6,\"button\"],[3,\"action\",[[19,0,[]],\"start\"]],[7],[0,\"Restart\"],[8],[0,\"\\n\"]],\"parameters\":[]},null]],\"parameters\":[]},{\"statements\":[[4,\"if\",[[20,[\"desktop\"]]],null,{\"statements\":[[0,\"        \"],[6,\"button\"],[3,\"action\",[[19,0,[]],\"start\"]],[7],[0,\"Start\"],[8],[0,\"\\n\"]],\"parameters\":[]},null]],\"parameters\":[]}],[6,\"canvas\"],[9,\"id\",\"stage\"],[9,\"width\",\"380\"],[9,\"height\",\"380\"],[7],[8]],\"hasEval\":false}", "meta": { "moduleName": "week-04/templates/components/tic-tac-toe.hbs" } });
});
define("week-04/templates/game", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "fF5EopNh", "block": "{\"symbols\":[],\"statements\":[[0,\"\\n\\n\"],[1,[18,\"Tic-Tac-Toe\"],false]],\"hasEval\":false}", "meta": { "moduleName": "week-04/templates/game.hbs" } });
});
define("week-04/templates/highscores", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "6JHpHJAB", "block": "{\"symbols\":[\"item\"],\"statements\":[[6,\"div\"],[9,\"class\",\"test-center\"],[7],[0,\"\\n    \"],[6,\"h2\"],[7],[0,\"High-scores\"],[8],[0,\"\\n\"],[8],[0,\"\\n\"],[6,\"ol\"],[7],[0,\"\\n\"],[4,\"each\",[[20,[\"model\"]]],null,{\"statements\":[[0,\"        \"],[6,\"li\"],[7],[0,\"\\n            \"],[6,\"div\"],[9,\"class\",\"float-left\"],[7],[1,[19,1,[\"name\"]],false],[8],[0,\"\\n            \"],[6,\"div\"],[9,\"class\",\"float-right\"],[7],[1,[19,1,[\"score\"]],false],[8],[0,\"\\n        \"],[8],[0,\"\\n\"]],\"parameters\":[1]},null],[8]],\"hasEval\":false}", "meta": { "moduleName": "week-04/templates/highscores.hbs" } });
});


define('week-04/config/environment', [], function() {
  var prefix = 'week-04';
try {
  var metaName = prefix + '/config/environment';
  var rawConfig = document.querySelector('meta[name="' + metaName + '"]').getAttribute('content');
  var config = JSON.parse(unescape(rawConfig));

  var exports = { 'default': config };

  Object.defineProperty(exports, '__esModule', { value: true });

  return exports;
}
catch(err) {
  throw new Error('Could not read config from meta tag with name "' + metaName + '".');
}

});

if (!runningTests) {
  require("week-04/app")["default"].create({"name":"week-04","version":"0.0.0+3d6e19f4"});
}
//# sourceMappingURL=week-04.map
