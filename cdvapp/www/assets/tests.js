'use strict';

define('week-04/tests/app.lint-test', [], function () {
  'use strict';

  QUnit.module('ESLint | app');

  QUnit.test('adapters/application.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'adapters/application.js should pass ESLint\n\n');
  });

  QUnit.test('app.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'app.js should pass ESLint\n\n');
  });

  QUnit.test('components/number-game.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/number-game.js should pass ESLint\n\n');
  });

  QUnit.test('components/tic-tac-toe.js', function (assert) {
    assert.expect(1);
    assert.ok(false, 'components/tic-tac-toe.js should pass ESLint\n\n13:9 - \'createjs\' is not defined. (no-undef)\n14:9 - \'createjs\' is not defined. (no-undef)\n15:9 - \'createjs\' is not defined. (no-undef)\n19:16 - \'shake\' is not defined. (no-undef)\n20:17 - \'shake\' is not defined. (no-undef)\n29:25 - \'createjs\' is not defined. (no-undef)\n31:25 - \'createjs\' is not defined. (no-undef)\n50:36 - \'createjs\' is not defined. (no-undef)\n59:35 - \'createjs\' is not defined. (no-undef)\n76:9 - \'createjs\' is not defined. (no-undef)\n81:12 - \'shake\' is not defined. (no-undef)\n82:13 - \'shake\' is not defined. (no-undef)\n98:21 - \'createjs\' is not defined. (no-undef)\n157:21 - \'createjs\' is not defined. (no-undef)\n187:21 - \'createjs\' is not defined. (no-undef)\n188:21 - \'createjs\' is not defined. (no-undef)\n190:17 - \'createjs\' is not defined. (no-undef)\n191:17 - \'createjs\' is not defined. (no-undef)\n194:17 - \'createjs\' is not defined. (no-undef)\n207:17 - \'markers\' is already defined. (no-redeclare)\n208:21 - \'idx\' is already defined. (no-redeclare)');
  });

  QUnit.test('controllers/game.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'controllers/game.js should pass ESLint\n\n');
  });

  QUnit.test('models/highscore.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/highscore.js should pass ESLint\n\n');
  });

  QUnit.test('resolver.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'resolver.js should pass ESLint\n\n');
  });

  QUnit.test('router.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'router.js should pass ESLint\n\n');
  });

  QUnit.test('routes/game.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/game.js should pass ESLint\n\n');
  });

  QUnit.test('routes/highscores.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/highscores.js should pass ESLint\n\n');
  });
});
define('week-04/tests/helpers/destroy-app', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = destroyApp;
  function destroyApp(application) {
    Ember.run(application, 'destroy');
  }
});
define('week-04/tests/helpers/module-for-acceptance', ['exports', 'qunit', 'week-04/tests/helpers/start-app', 'week-04/tests/helpers/destroy-app'], function (exports, _qunit, _startApp, _destroyApp) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  exports.default = function (name) {
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    (0, _qunit.module)(name, {
      beforeEach: function beforeEach() {
        this.application = (0, _startApp.default)();

        if (options.beforeEach) {
          return options.beforeEach.apply(this, arguments);
        }
      },
      afterEach: function afterEach() {
        var _this = this;

        var afterEach = options.afterEach && options.afterEach.apply(this, arguments);
        return Ember.RSVP.resolve(afterEach).then(function () {
          return (0, _destroyApp.default)(_this.application);
        });
      }
    });
  };
});
define('week-04/tests/helpers/start-app', ['exports', 'week-04/app', 'week-04/config/environment'], function (exports, _app, _environment) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = startApp;
  function startApp(attrs) {
    var attributes = Ember.merge({}, _environment.default.APP);
    attributes.autoboot = true;
    attributes = Ember.merge(attributes, attrs); // use defaults, but you can override;

    return Ember.run(function () {
      var application = _app.default.create(attributes);
      application.setupForTesting();
      application.injectTestHelpers();
      return application;
    });
  }
});
define('week-04/tests/test-helper', ['week-04/app', 'week-04/config/environment', '@ember/test-helpers', 'ember-qunit'], function (_app, _environment, _testHelpers, _emberQunit) {
  'use strict';

  (0, _testHelpers.setApplication)(_app.default.create(_environment.default.APP));

  (0, _emberQunit.start)();
});
define('week-04/tests/tests.lint-test', [], function () {
  'use strict';

  QUnit.module('ESLint | tests');

  QUnit.test('helpers/destroy-app.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'helpers/destroy-app.js should pass ESLint\n\n');
  });

  QUnit.test('helpers/module-for-acceptance.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'helpers/module-for-acceptance.js should pass ESLint\n\n');
  });

  QUnit.test('helpers/start-app.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'helpers/start-app.js should pass ESLint\n\n');
  });

  QUnit.test('test-helper.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'test-helper.js should pass ESLint\n\n');
  });
});
require('week-04/tests/test-helper');
EmberENV.TESTS_FILE_LOADED = true;
//# sourceMappingURL=tests.map
